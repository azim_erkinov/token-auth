﻿using Microsoft.AspNetCore.Identity;

namespace Token_BasedAuth.Data.Models
{
    public class ApplicationUser:IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; } 
        public string Custome { get; set; }
    }
}
