﻿using System.ComponentModel.DataAnnotations;

namespace Token_BasedAuth.Data.ViewModels
{
    public class LoginVM
    {
        [Required]
        public string EmailAddress { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
